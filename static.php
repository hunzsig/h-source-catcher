<?php
require 'static/array.php';
require 'static/is.php';
require 'static/datetime.php';
require 'static/parse.php';
require 'static/curl.php';
require 'static/filter.php';
require 'static/system.php';
require 'static/windows.php';

abstract class Decoder
{
    protected $downloadDir = __DIR__ . DIRECTORY_SEPARATOR . 'download' . DIRECTORY_SEPARATOR;
    protected $pageMax = array();
    protected $pageCount = array();

    private function show($str, $color = null)
    {
        $Color = (new Color());
        $isTerminal = (empty($_COOKIE)) ? true : false;
        $str = '->' . $str;
        switch ($color) {
            case 'red':
                echo (!$isTerminal) ? "<b style='color: red'>{$str}</b>" : $Color::lightRed($str);
                break;
            case 'yellow':
                echo (!$isTerminal) ? "<b style='color: #ffaf37'>{$str}</b>" : $Color::lightYellow($str);
                break;
            case 'blue':
                echo (!$isTerminal) ? "<b style='color: dodgerblue'>{$str}</b>" : $Color::lightBlue($str);
                break;
            case 'green':
                echo (!$isTerminal) ? "<b style='color: green'>{$str}</b>" : $Color::lightGreen($str);
                break;
            case 'grey':
                echo (!$isTerminal) ? "<b style='color: lightgrey'>{$str}</b>" : $Color::darkGray($str);
                break;
            default:
                echo $str;
                break;
        }
        echo PHP_EOL;
    }

    protected function tips($str)
    {
        $this->show("<TIPS>" . $str, 'blue');
    }

    protected function miss($str)
    {
        $this->show("<MISS>" . $str, 'grey');
    }

    protected function alert($str)
    {
        $this->show("<ALERT>" . $str, 'yellow');
    }

    protected function success($str)
    {
        $this->show("<SUCCESS>" . $str, 'green');
    }

    protected function error($str)
    {
        $this->show("<ERROR>" . $str, 'red');
        winSound("出错了");
    }

    private function Rand_IP()
    {
        $ip2id = round(rand(600000, 2550000) / 10000); //第一种方法，直接生成
        $ip3id = round(rand(600000, 2550000) / 10000);
        $ip4id = round(rand(600000, 2550000) / 10000);
        $arr_1 = array("218", "218", "66", "66", "218", "218", "60", "60", "202", "204", "66", "66", "66", "59", "61", "60", "222", "221", "66", "59", "60", "60", "66", "218", "218", "62", "63", "64", "66", "66", "122", "211");
        $randarr = mt_rand(0, count($arr_1) - 1);
        $ip1id = $arr_1[$randarr];
        return $ip1id . "." . $ip2id . "." . $ip3id . "." . $ip4id;
    }

    /**
     * 使用PHP检测能否ping通IP或域名
     * @param type $address
     * @return boolean
     */
    protected function pingAddress($address)
    {
        $status = -1;
        if (strcasecmp(PHP_OS, 'WINNT') === 0) {
            // Windows 服务器下
            $pingresult = exec("ping -n 1 {$address}", $outcome, $status);
        } elseif (strcasecmp(PHP_OS, 'Linux') === 0) {
            // Linux 服务器下
            $pingresult = exec("ping -c 1 {$address}", $outcome, $status);
        }
        var_dump($status);
        return 0 === $status;
    }

    public function checkAddress($ip, $port)
    {
        $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_nonblock($sock);
        socket_connect($sock, $ip, $port);
        socket_set_block($sock);
        $r = array($sock);
        $w = array($sock);
        $f = array($sock);
        $status = socket_select($r, $w, $f, 5);
        return $status == 1;
    }


    protected function curl($url)
    {
        if (!is_file(__DIR__ . DIRECTORY_SEPARATOR . 'cookie.txt')) {
            file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'cookie.txt', '');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // 这样能够让cURL支持页面链接跳转
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-FORWARDED-FOR:' . $this->Rand_IP(), 'CLIENT-IP:' . $this->Rand_IP()));
        if ($this->checkAddress("127.0.0.1", 1080)) {
            curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 0);
            curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1');
            curl_setopt($ch, CURLOPT_PROXYPORT, '1080');
            curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);
        }
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, realpath(__DIR__ . DIRECTORY_SEPARATOR . 'cookie.txt'));
        curl_setopt($ch, CURLOPT_COOKIEFILE, realpath(__DIR__ . DIRECTORY_SEPARATOR . 'cookie.txt'));
        curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . session_id());
        $output = curl_exec($ch);
        if ($output === false) {
            $error = curl_error($ch);
            if (in_array($error, [
                'Empty reply from server'
            ])) {
                $this->tips($error);
            } else {
                $this->error((curl_error($ch)));
            }
        }
        curl_close($ch);
        return $output;
    }

    protected function getPagesHost($url)
    {
        $host = explode('/', $url);
        return $host[2];
    }

    protected function getPagesBaseUrl($url)
    {
        $u = explode('://', $url);
        $u = array_pop($u);
        $u = explode('?', $u);
        return current($u);
    }

    protected function initPagesCount($url)
    {
        $u = $this->getPagesBaseUrl($url);
        $this->pageMax[$u] = 9999999;
        $this->pageCount[$u] = 0;
    }

    protected function checkOver($count, $max)
    {
        if ($count >= $max) {
            $this->alert('finished!');
            winSound("下完了");
            G('s2');
            $this->alert('耗时：' . G('s1', 's2', 10));
        }
    }

    protected function mkMyDir($name, $baseUrl)
    {
        $dir = $this->downloadDir . $name;
        if (!is_dir($dir)) {
            try {
                mkdir($dir);
                chmod($dir, 0777);
            } catch (\Exception $e) {
                $this->error('mkdir error');
                return false;
            }
        }
        $dir = realpath($dir);
        @file_put_contents($dir . DIRECTORY_SEPARATOR . 'url.log', date('Y-m-d H:i:s') . ' -> ' . $baseUrl . PHP_EOL, FILE_APPEND);
        return $dir;
    }

    protected function mikananiMe($baseUrl, $func)
    {
        $this->alert('URL: ' . $baseUrl);
        $text = $this->curl($baseUrl);
        $text = str_replace(["\r", "\n"], '', $text);
        // 提取目录名
        preg_match('/<p class="bangumi-title">(.*?)<a href/', $text, $name);
        $name = trim($name[1]);
        $name = str_replace(['\\', '/', ':', '*', '?', '"', '<', '>', '|'], '', $name);
        if (!$name) {
            $this->error('catch name');
        }
        // 创建目录
        $dir = $this->mkMyDir($name, $baseUrl);
        $this->success('NAME: ' . $name);
        $this->tips('DIR: ' . $dir);
        preg_match_all('/<tr>(.*?)<\/tr>/', $text, $tds);
        $tds = $tds[1];
        $tHost = parse_url($baseUrl);
        $tHost = $tHost['scheme'] . '://' . $tHost['host'];
        $torrentJudge = array();
        $torrentList = array();
        foreach ($tds as $t) {
            preg_match('/<a href="(.*?)\.torrent"><img/', $t, $torrent);
            preg_match('/<a class="magnet-link-wrap" target="_blank" href="(.*?)">(.*?)<\/a>/', $t, $tname);
            $torrent = $torrent[1] ?? null;
            $tname = $tname[2] ?? null;
            $tname = str_replace(['\\', '/', ':', '*', '?', '"', '<', '>', '|'], '', $tname);
            if (!$torrent || !$tname) continue;
            if (!in_array($torrent, $torrentJudge)) {
                $torrentJudge[] = $torrent;
                $torrentList[] = array(
                    'name' => $tname . '.torrent',
                    'torrent' => $tHost . $torrent . '.torrent',
                );
            }
        }
        $u = $this->getPagesBaseUrl($baseUrl);
        $pages = count($torrentList);
        if ($this->pageMax[$u] === 9999999) {
            $this->pageMax[$u] = $pages;
        }
        foreach ($torrentList as $k => $p) {
            $fName = $p['name'] ?? null;
            $fileUri = $dir . DIRECTORY_SEPARATOR . $fName;
            if (is_file($fileUri)) {
                $this->pageCount[$u] += 1;
                $this->miss("URI:[{$this->pageCount[$u]}/{$this->pageMax[$u]}]{$fileUri}");
                $this->checkOver($this->pageCount[$u], $this->pageMax[$u]);
                continue;
            }
            if ($func) {
                $func($p['torrent'], $dir, $p['name']);
            }
        }
    }

    protected function eHentaiOrg($baseUrl, $func)
    {
        $this->alert('URL: ' . $baseUrl);
        $text = $this->curl($baseUrl);
        // 警告跳过
        if (strpos($text, 'View Gallery') !== false) {
            preg_match('/<a href="(.*?)">Never Warn Me Again<\/a>/', $text, $gallery);
            $gallery = $gallery[1] ?? null;
            if (!$gallery) {
                $this->error('View Gallery fail');
            }
            $text = $this->curl($gallery);
            $text = str_replace(array("\t", "\n", "\r", "\r\n", "<br\/>", "<br>"), ' ', $text);
        }
        // 提取目录名
        preg_match('/<h1 id="gj">(.*?)<\/h1>/', $text, $name);
        $name = $name[1];
        $name = str_replace(['\\', '/', ':', '*', '?', '"', '<', '>', '|'], '', $name);
        if (!$name) {
            preg_match('/<h1 id="gn">(.*?)<\/h1>/', $text, $name);
            $name = $name[1];
            $name = str_replace(['\\', '/', ':', '*', '?', '"', '<', '>', '|'], '', $name);
            if (!$name) {
                $this->error('catch name');
            }
        }
        // 创建目录
        $dir = $this->mkMyDir($name, $baseUrl);
        $this->success('NAME: ' . $name);
        $this->tips('DIR: ' . $dir);
        preg_match('/<div id="gdt">(.*)<div class="c"><\/div>/', $text, $picsText);
        $picsText = current($picsText);
        preg_match_all('/<a href="(.*?)"><img alt="(.*?)" title="(.*?)"(.*?)<\/a>/', $picsText, $pics);
        $picsName = $pics[3];
        $pics = $pics[1];
        foreach ($picsName as $k => $p) {
            $p = explode(':', $p);
            $picsName[$k] = trim(array_pop($p));
        }
        $u = $this->getPagesBaseUrl($baseUrl);
        preg_match('/Length:<\/td><td class="gdt2">(.*?) pages<\/td>/', $text, $pages);
        $pages = intval(trim($pages[1]));
        if ($this->pageMax[$u] === 9999999) {
            $this->pageMax[$u] = $pages;
        }
        $pages = ceil($pages / 40);
        foreach ($pics as $k => $p) {
            $fName = $picsName[$k] ?? null;
            $fileUri = $dir . DIRECTORY_SEPARATOR . $fName;
            if (is_file($fileUri)) {
                $this->pageCount[$u] += 1;
                $this->miss("URI:[{$this->pageCount[$u]}/{$this->pageMax[$u]}]{$fileUri}");
                $this->checkOver($this->pageCount[$u], $this->pageMax[$u]);
                continue;
            }
            if ($func) {
                $func($p, $dir);
            }
        }
        // 提取总图片数量，一页40(自动搜后面的其他页)
        if (strpos($baseUrl, '?p=') === false) {
            $start = 1;
        } else {
            $start = explode('?p=', $baseUrl);
            $start = array_pop($start);
            $start = intval(trim($start));
            $start = $start + 1;
        }
        if ($start < $pages) {
            $picHost = parse_url($baseUrl);
            $this->eHentaiOrg($picHost['scheme'] . '://' . $u . '?p=' . $start, $func);
        }
    }

    protected function asmhentaiCom($baseUrl, $func)
    {
        $this->alert('URL: ' . $baseUrl);
        $text = $this->curl($baseUrl);
        // 提取目录名
        preg_match('/<h2>(.*?)<\/h2>/', $text, $name);
        $name = $name[1];
        $name = str_replace(['\\', '/', ':', '*', '?', '"', '<', '>', '|'], '', $name);
        if (!$name) {
            preg_match('/<h1>(.*?)<\/h1>/', $text, $name);
            $name = $name[1];
            $name = str_replace(['\\', '/', ':', '*', '?', '"', '<', '>', '|'], '', $name);
            if (!$name) {
                $this->error('catch name');
            }
        }
        // 创建目录
        $dir = $this->mkMyDir($name, $baseUrl);
        $this->success('NAME: ' . $name);
        $this->tips('DIR: ' . $dir);
        $u = $this->getPagesBaseUrl($baseUrl);
        preg_match('/<h3>Pages: (.*?)<\/h3>/', $text, $pages);
        $pages = intval(trim($pages[1]));
        if ($this->pageMax[$u] === 9999999) {
            $this->pageMax[$u] = $pages;
        }
        $picHost = str_replace('/g/', '/gallery/', $baseUrl);
        for ($i = 1; $i <= $this->pageMax[$u]; $i++) {
            $fName = $i . '.jpg';
            $fileUri = $dir . DIRECTORY_SEPARATOR . $fName;
            if (is_file($fileUri)) {
                $this->pageCount[$u] += 1;
                $this->miss("URI:[{$this->pageCount[$u]}/{$this->pageMax[$u]}]{$fileUri}");
                $this->checkOver($this->pageCount[$u], $this->pageMax[$u]);
                continue;
            }
            if ($func) {
                $func($picHost . $i . '/', $dir);
            }
        }
    }
}
