<?php
date_default_timezone_set('PRC');
require 'static.php';
require 'hColor.php';

class Main extends Decoder
{
    private $server = null;

    public function __construct($port)
    {
        //
        $this->server = new swoole_http_server("0.0.0.0", $port);
        $this->server->set(array(
            'worker_num' => PHP_OS === 'CYGWIN' ? 2 : 4,
            'task_worker_num' => PHP_OS === 'CYGWIN' ? 4 : 8,
        ));
        $this->server->on("start", function () {
            echo "server start" . PHP_EOL;
        });
        $this->server->on("workerStart", function ($worker) {
            // echo "worker start" . PHP_EOL;
        });
        $this->server->on("request", function ($request, $response) {
            $status = null;
            if ($request->get) {
                $status = $this->catcher($request->get['url']);
            }
            if ($status) {
                $response->end('download...ing...');
            } else {
                $response->end('download fail!');
            }
        });
        $this->server->on('task', function ($server, $task_id, $from_id, $request) {
            $url = $request['url'];
            $host = $request['host'];
            $p = $request['p'];
            $dir = $request['dir'];
            $fName = $request['fName'] ?? null;
            $src = null;
            //
            switch ($host) {
                case 'mikanani.me':
                    $src = $p;
                    break;
                case 'e-hentai.org':
                    $pText = $this->curl($p);
                    preg_match('/<img id="img" src="(.*?)" (.*)>/', $pText, $src);
                    $src = $src[1] ?? null;
                    break;
                case 'asmhentai.com':
                    $pText = $this->curl($p);
                    $pText = str_replace(PHP_EOL, '', $pText);
                    preg_match('/<div id="img"(.*?)src="(.*?)"(.*?)<\/div>/', $pText, $sss);
                    if ($sss) {
                        $sss = $sss[2];
                    }
                    if ($sss) {
                        $src = 'https:' . $sss;
                    }
                    break;
                default:
                    break;
            }
            $u = $this->getPagesBaseUrl($url);
            $data = array(
                'u' => $u,
            );
            if ($src) {
                if (!$fName) {
                    $fName = explode('/', $src);
                    $fName = array_pop($fName);
                }
                $fileUri = $dir . DIRECTORY_SEPARATOR . $fName;
                if (!is_file($fileUri)) {
                    $this->tips('SRC: ' . $src);
                    $fdata = $this->curl($src);
                    if ($fdata) {
                        try {
                            @file_put_contents($fileUri, $fdata);
                            @chmod($fileUri, 0777);
                            if (is_file($fileUri)) {
                                $data['status'] = 'SUCCESS';
                            } else {
                                $data['status'] = 'FAIL';
                            }
                        } catch (\Exception $e) {
                            $this->error($e);
                            $data['status'] = 'ERROR';
                        }
                    } else {
                        $this->error('empty fdata');
                        $data['status'] = 'ERROR';
                    }
                }
                $data['fileUri'] = $fileUri;
                $data['name'] = $fName;
            }
            return $data;
        });
        $this->server->on('finish', function ($server, $task_id, $data) {
            // echo "AsyncTask Finish" . PHP_EOL;
        });

        $this->server->start();
    }

    public function catcher($url)
    {
        if (!$url) {
            return false;
        }
        G('s1');
        $this->initPagesCount($url);
        $host = $this->getPagesHost($url);
        $func = function ($p, $dir, $fName = null) use ($url, $host) {
            $request = array(
                'url' => $url,
                'host' => $host,
                'dir' => $dir,
                'p' => $p,
                'fName' => $fName,
            );
            $this->server->task($request, -1, function ($server, $task_id, $result) {
                $u = $result['u'];
                $this->pageCount[$u] += 1;
                $status = $result['status'] ?? null;
                switch ($status) {
                    case 'SUCCESS':
                        $this->success("DOWNLOAD:[{$this->pageCount[$u]}/{$this->pageMax[$u]}]{$result['fileUri']}");
                        break;
                    case 'FAIL':
                        $this->miss("FAIL:[{$this->pageCount[$u]}/{$this->pageMax[$u]}]{$result['fileUri']}");
                        break;
                    default:
                        $this->error("ERR:[{$this->pageCount[$u]}/{$this->pageMax[$u]}]{$result['fileUri']}");
                        break;
                }
                $this->checkOver($this->pageCount[$u], $this->pageMax[$u]);
            });
        };
        //
        $status = true;
        switch ($host) {
            case 'mikanani.me':
                $this->mikananiMe($url, $func);
                break;
            case 'e-hentai.org':
                $this->eHentaiOrg($url, $func);
                break;
            case 'asmhentai.com':
                $this->asmhentaiCom($url, $func);
                break;
            default:
                $status = false;
                $this->error('尚不支持此网站');
                break;
        }
        return $status;
    }

}

$params = getopt('p:');
if (!$params['p']) exit('must set port with -p!');

$main = new Main($params['p']);