<?php
date_default_timezone_set('PRC');
require 'static.php';
require 'hColor.php';

class Main extends Decoder
{

    private $url = null;

    public function __construct($url)
    {
        $this->url = $url;
        if (!is_dir(__DIR__ . DIRECTORY_SEPARATOR . 'download')) {
            @mkdir(__DIR__ . DIRECTORY_SEPARATOR . 'download');
        }
    }

    public function catcher()
    {
        if (!$this->url) exit('not url');
        G('s1');
        //
        $url = $this->url;
        $this->initPagesCount($url);
        $host = $this->getPagesHost($url);
        $func = function ($p, $dir, $fName = null) use ($url, $host) {
            $src = null;
            //
            switch ($host) {
                case 'mikanani.me':
                    $src = $p;
                    break;
                case 'e-hentai.org':
                    $pText = $this->curl($p);
                    preg_match('/<img id="img" src="(.*?)" (.*)>/', $pText, $src);
                    $src = $src[1];
                    break;
                case 'asmhentai.com':
                    $pText = $this->curl($p);
                    preg_match('/<img class="lazy no_image" alt="(.*?)" src="(.*?)" \/>/', $pText, $src);
                    if (!$src) {
                        preg_match('/<img src="(.*?)" alt="" \/><\/div>/', $pText, $src);
                        $src = 'https:' . $src[1];
                    } else {
                        $src = 'https:' . $src[2];
                    }
                    break;
                default:
                    break;
            }
            if ($src) {
                if (!$fName) {
                    $fName = explode('/', $src);
                    $fName = array_pop($fName);
                }
                $fileUri = $dir . DIRECTORY_SEPARATOR . $fName;
                if (!is_file($fileUri)) {
                    $this->tips('SRC: ' . $src);
                    $fdata = $this->curl($src);
                    if ($fdata) {
                        try {
                            file_put_contents($fileUri, $fdata);
                            @chmod($fileUri, 0777);
                            $u = $this->getPagesBaseUrl($url);
                            $this->pageCount[$u] += 1;
                            $this->success("DOWNLOAD:[{$this->pageCount[$u]}/{$this->pageMax[$u]}]{$fileUri}");
                            $this->checkOver($this->pageCount[$u], $this->pageMax[$u]);
                        } catch (\Exception $e) {
                            $this->error($e);
                        }
                    }
                }
            }
        };
        //
        switch ($host) {
            case 'mikanani.me':
                $this->mikananiMe($url, $func);
                break;
            case 'e-hentai.org':
                $this->eHentaiOrg($url, $func);
                break;
            case 'asmhentai.com':
                $this->asmhentaiCom($url, $func);
                break;
            default:
                $this->error('尚不支持此网站');
                break;
        }
    }

}

$params = getopt('u:');
if (!$params['u']) exit('must set url with -u!');

$main = new Main($params['u']);
$main->catcher();